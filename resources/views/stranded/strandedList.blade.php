@extends('layouts/app')
@section('title','Stranded List')

@section('content')
  <div >


 
                    <table class="table mx-1" id="datatable">
                        <thead style="background-color: #2c6ac6; color: white;">
                            <tr>
                                <th scope="col"  width="5%">Id</th>
                                <th scope="col" width="10%">Name</th>
                                <th scope="col" width="10%">Fathers Name</th>
                                <!-- <th scope="col" width="23%">Password</th> -->
                                <th scope="col" width="10%"> Address(Mzr)</th>
                                <th scope="col" width="10%">District</th>
                                <th scope="col" width="10%">State</th>
                                <th scope="col" style="text-align: center;" width="10%">Address(Outside)</th>
                                <th scope="col" width="10%">Problem</th>
                            </tr>


                        </thead>
                        <tbody id="myTable">
                        @foreach($strandeds as $stranded)
                            <tr class="table-light">
                                <td scope="row">{{ $stranded->id }}</td>
                                <td>{{ $stranded->name }}</td>
                                <td>{{ $stranded->fname }}</td>

                                <td>{{ $stranded->mizoramaddress}}</td>
                                <td>{{ $stranded->mizoramdistrict }}</td>
                                <td>{{ $stranded->outsidestate }}</td>
                                <td>{{ $stranded->outsideaddress }}</td>
                                <td>{{ $stranded->problem }}</td>

                                
                                
                            </tr>
                        @endforeach

                        </tbody>
                    </table>
                </div>
                <div class="paginator container">

                    {{ $strandeds->appends($_GET)->onEachSide(1)->links() }}

                </div>



@endsection

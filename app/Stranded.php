<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Stranded extends Model
{
    protected $fillable = [
        'name', 'fname', 'phone','epic','mizoramaddress','mizoramdistrict','outsideaddress','outsidestate',
        'occupation','gender','problem',
    ];
}

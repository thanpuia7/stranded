<?php

namespace App\Http\Controllers;

use App\District;
use App\State;
use App\Stranded;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class StrandedController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $districts = District::orderBy('name','asc')->get();
        $states = State::orderBy('name','asc')->get();
        return view ('stranded.form',compact('districts','states'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $this->validate($request,[

            
            'name' => 'required',
            'fname' => 'required',
         // 'email' => 'required|email|max:255|unique:users',
            'phone' => 'required|regex:/[0-9]{10}/',
            'mizoramaddress' => 'required',
            'mizoramdistrict' => 'required',
            'outsideaddress' => 'required',
            'outsidestate' => 'required',
            'occupation' => 'required',
            'gender' => 'required',

            
            ]);

            if (Stranded::where('name', '=', $request->name)->exists() && Stranded::where('fname', '=', $request->fname)->exists() && Stranded::where('phone', '=', $request->phone)->exists()) {
                
                // dd("this is endered");
                return redirect()->back()->with('userexist', 'Warning!! You are already registered in our database, For more information please contact the number below'); 
             }

             if($request->email != null)
             {
                 return redirect()->back();
             }
             
             


            $stranded= new Stranded();
            
            $stranded->name = $request->name;
            $stranded->fname = $request->fname;
            $stranded->phone = $request->phone;
            $stranded->epic = $request->epic;
            $stranded->mizoramaddress = $request->mizoramaddress;
            $stranded->mizoramdistrict = $request->mizoramdistrict;
            $stranded->outsideaddress = $request->outsideaddress;
            $stranded->outsidestate = $request->outsidestate;
            $stranded->occupation = $request->occupation;
            $stranded->gender = $request->gender;
            $stranded->problem = $request->problem;

            $stranded->save();

            return redirect("/success");
    }

    public function success()
    {
        return view('stranded.success');
    }

    public function view()
    {
        $strandeds = Stranded::orderBy('id')->paginate();

        return view('stranded.strandedList',compact('strandeds'));
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

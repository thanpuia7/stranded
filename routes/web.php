<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Auth::routes();

// Route::get('/home', 'HomeController@index')->name('home');

Route::get('/', 'StrandedController@index')->name('2');
Route::get('/home', 'StrandedController@index')->name('home');

Route::get('/index', 'StrandedController@index')->name('home1');
Route::post('/strandedS', 'StrandedController@create')->name('strandedSubmit');

Route::get('/success','StrandedController@success');

Route::get('/list', 'StrandedController@view')->name('strandedView')->middleware('IsAdmin');
